//alert('ok');

var dados = [
    {"pilha": "Dura plus LRO3 AAA", "tensao_nominal": 1.5, "corrente": 1200, "E": 0.988, "V": 0.847, "R": 23.7},
    {"pilha": "Philips AAA LRO3", "tensao_nominal": 1.5, "corrente": 1200, "E": 1.369, "V": 1.290, "R": 23.7},
    {"pilha": "Panasonic AA LR6/LRXA", "tensao_nominal": 1.5, "corrente": 2000, "E": 1.379, "V": 1.313, "R": 23.7},
    {"pilha": "Duracell AA MN1599 1.5V LR6", "tensao_nominal": 1.5, "corrente": 2000, "E": 1.305, "V": 1.289, "R": 23.7},
    {"pilha": "Luatek GH 18650 1200mAh 3.7V", "tensao_nominal": 3.7, "corrente": 1200, "E": 2.446, "V": 2.336, "R": 23.7},
    {"pilha": "JYX 9800mAh SD 18650 3.7V-4.2V li-ion", "tensao_nominal": 3.7, "corrente": 9800, "E": 2.446, "V": 2.440, "R": 23.7},
    {"pilha": "Elgin 9V 250mAh", "tensao_nominal": 9, "corrente": 250, "E": 0.720, "V": 1.995, "R": 23.7},
    {"pilha": "Golite 9V 6F22 216 HD S006P 1604D", "tensao_nominal": 9, "corrente": 500, "E": 5.86, "V": 2.732, "R": 23.7},
    {"pilha": "Unipower Unicobra 12V 7.0Ah- F187", "tensao_nominal": 12, "corrente": 7000, "E": 10.49, "V": 10.36, "R": 23.7},
    {"pilha": "Freedom Liberdade 12V C-100 30Ah/C-20 26Ah/ C-10 24Ah", "tensao_nominal": 12, "corrente": 30000, "E": 10.68, "V": 10.50, "R": 23.7},
    ];

    function calcularResistencia() {
        var tabela = document.getElementById("tbDados");

        var tabelaHTML = `<tr>
        <th>Pilha/Bateria</th>
        <th>Tensão nominal (V)</th>
        <th>Capacidade de corrente (mA.h)</th>
        <th>Tensão sem carga(V)</th>
        <th>Tensão com carga(V)</th>
        <th>Resitência de carga(ohm)</th>
        <th>Resistência interna(ohm)</th>
    </tr>`;

    for (var i = 0; i < dados.length; i++) {
        var linha = dados[i];
        var E = linha.E;
        var V = linha.V;
        var R = linha.R;
        var r = R * (E / V - 1);

        var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";

        tabelaHTML += novaLinha;
    }
    tabela.innerHTML = tabelaHTML;
}

calcularResistencia();
