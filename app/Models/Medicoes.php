<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class medicoes extends Model
{
    use HasFactory;
    public function getResistenciaInternaAttribute()
    {
        return $this->resistencia_carga*($this->tensao_sem_carga / $this->tensao_com_carga -1 );
    }
}
