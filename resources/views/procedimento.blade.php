@extends('templates.base') 
@section('conteudo')







<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Baterias</title>


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../css/estilos.css">
</head>
<body>
    <header>
        <h1>Baterias</h1>
    </header>

    <nav>
        <ul>
            <li><a href="../index.html">voltar para página principal</a></li>
        </ul>
    </nav>

    <main>
        <h1>Procedimento</h1>
        <hr>
        <h2>Medições:</h2>
        <p>
            Durante a aula de eletroeletrônica/programação em automação do dia 14/07, realizamos as medições das tensões de cada bateria/pilha escolhida pelo professor para realização do trabalho, o primeiro
            passo foi medir as pilhas e logo depois as baterias utilizando o multímetro, logo depois começamos as medições das baterias/pilhas com o Registor e para medir com o Registor precisamos mudar de
            resistência para tensão, anotamos os resultados da medições e conseguimos concluir que a diferença que com o Registor a pilha/bateria descarrega(cai pra zero mais rápido) e da uma estabilidade.
        </p>
        <h3>Tabela de medições:</h3>
        <p>
            Usando o código java do professor como exemplo nós montamos um código capaz de preencher a tabela com as imformações de medição das pilhas e baterias. Para não precisarmos fazer o calculo manual das
            resistências internas das pilhas e baterias nós usamos o mesmo código para fazer o cálculo e automaticamente o imprimir na tabela com o resto das informações.
        </p>
    </main>
</body>
</html>
@endsection

@section('rodape')
    
<h4>Procedimento</h4>

@endsection 