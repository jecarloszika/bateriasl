<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class MedicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('medicoes')->insert(

            [
                
                [
                    'pilha_bateria'=> 'pilha XXX1',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente'=>2800,
                    'tensao_sem_carga'=>1.304,
                    'tensao_com_carga'=>1,
                    'resistencia_carga'=>1,
                ],
                [
                    'pilha_bateria'=> 'pilha XXX2 ',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente'=>2800,
                    'tensao_sem_carga'=>1.304,
                    'tensao_com_carga'=>1,
                    'resistencia_carga'=>1,
                ]
            
            
            ]);


        
    }
}
